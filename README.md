To replicate the site for the RO/GSAC archives.

* Create a directory (ex. archives)
* cd archives
* git clone https://git.uwaterloo.ca/wcms/uw_ro_gsac_archive .
* mkdir graduate-studies
    * cd graduate-studies
    * git clone https://git.uwaterloo.ca/wcms/gsac_archive .
* cd ..
* mkdir undergraduate-studies
    * cd undergraduate-studies
    * git clone https://git.uwaterloo.ca/wcms/ro_archiving .
* cd ..
* ln -s undergraduate-studies/App_Themes/ App_Themes
* ln -s graduate-studies/fonts/ fonts
